"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = union;
/**
 * Vrati zjednotenie poly bez duplicit (polia musia byt zoradene)
 *
 * @param {Array} a zoradene pole
 * @param {Array} b zoradene pole
 * @returns {Array} zjednotenie poly bez duplicit
 */
function union(a, b) {
  var i = 0;
  var j = 0;

  var iMax = a.length;
  var jMax = b.length;

  var result = [];
  var last = null;
  var actual = void 0;
  while (i < iMax || j < jMax) {
    if (i < iMax) {
      if (j < jMax) {
        // oboje polia
        if (a[i] < b[j]) {
          // a ma mensi prvok
          actual = a[i++];
        } else if (a[i] > b[j]) {
          // b ma mensi prvok
          actual = b[j++];
        } else {
          // rovnake prvky (mozme spravovat naraz)
          actual = a[i++];
          j++;
        }
      } else {
        // iba a
        actual = a[i++];
      }
    } else {
      // iba b
      actual = b[j++];
    }

    if (last === null || last !== actual) {
      result.push(actual);
      last = actual;
    }
  }

  return result;
}