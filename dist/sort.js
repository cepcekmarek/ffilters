'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _urijs = require('urijs');

var _urijs2 = _interopRequireDefault(_urijs);

var _swap = require('./swap');

var _swap2 = _interopRequireDefault(_swap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sort = function () {
  function Sort(container) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Sort);

    this.values = [];
    this.valuesMap = {};

    this.container = container;

    this.renderValue = function (title, active) {
      return title;
    };

    this.onChange = [];

    this.defaultValueId = null;
    this.activeValueId = null;

    this.idsPropName = 'ids';
    this.valueIdPropName = 'id';

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.setData(options); // nastavi hodnoty pri inicializaci
  }

  _createClass(Sort, [{
    key: 'setData',
    value: function setData(data) {
      var _this = this;

      Object.assign(this, data);

      this.valuesMap = {};
      this.values.forEach(function (value) {
        _this.valuesMap[value[_this.valueIdPropName]] = value;
      });

      return this;
    }
  }, {
    key: 'getActiveValue',
    value: function getActiveValue() {
      return this.valuesMap[this.getActiveValueId()];
    }
  }, {
    key: 'getActiveValueId',
    value: function getActiveValueId() {
      return this.activeValueId || this.defaultValueId;
    }
  }, {
    key: 'getDisabledFilters',
    value: function getDisabledFilters() {
      return this.valuesMap[this.getActiveValueId()].disabledFilters || [];
    }
  }, {
    key: 'setActiveValueId',
    value: function setActiveValueId(valueId) {
      this.activeValueId = valueId;

      return this;
    }
  }, {
    key: 'updateUrl',
    value: function updateUrl() {
      var activeValueId = this.getActiveValueId();
      var uri = new _urijs2.default(window.location.href);

      if (activeValueId != this.defaultValueId) {
        // neni prazdnu query a nieje defaultna hodnota
        uri.setQuery('s', activeValueId);
      } else {
        uri.removeQuery('s');
      }

      window.history.pushState(null, null, uri.toString());

      return this;
    }
  }, {
    key: 'invalidate',
    value: function invalidate() {
      this.onChange.forEach(function (callback) {
        callback();
      });

      return this;
    }
  }, {
    key: 'applySort',
    value: function applySort(values) {
      var activeValue = this.getActiveValue();

      if (!activeValue) {
        // neni nastavena aktivna hodnota nic nezoradi
        return values;
      }

      if (!values) {
        return activeValue[this.idsPropName]; // neni su nastavene vstupne data tak vrati vsetky idecka z razeni
      }

      var swapedValues = activeValue.swapedValues || (activeValue.swapedValues = (0, _swap2.default)(activeValue[this.idsPropName]));

      values = values.slice(); // naklonuje pole aby nemodifikovalo povodne

      values.sort(function (a, b) {
        return swapedValues[a] - swapedValues[b];
      });

      return values;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      this.onRenderStart.forEach(function (callback) {
        callback();
      });

      var activeValueId = this.getActiveValueId();

      this.container.innerHTML = '';

      this.values.forEach(function (value) {
        var active = value[_this2.valueIdPropName] == activeValueId;

        var sortCallback = function sortCallback(event) {
          event.preventDefault();
          if (!active) {
            _this2.setActiveValueId(value[_this2.valueIdPropName]).updateUrl().invalidate().render();
          }
        };

        _this2.renderValue(value, _this2.container, sortCallback, active);
      });

      this.onRenderEnd.forEach(function (callback) {
        callback();
      });

      return this;
    }
  }]);

  return Sort;
}();

exports.default = Sort;