"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = intersect;

/**
 * Veati prienik zadanych poly bez duplicit (polia musia byt zoradene)
 *
 * @param {Array} a
 * @param {Array} b
 * @returns {Array} prienik poly
 */
function intersect(a, b) {
  var i = 0;
  var j = 0;

  var iMax = a.length;
  var jMax = b.length;

  var result = [];
  var last = null;
  var actual = void 0;
  while (i < iMax && j < jMax) {
    if (a[i] < b[j]) {
      i++;
    } else if (a[i] > b[j]) {
      j++;
    } else {
      actual = a[i++];
      j++;

      if (last === null || last !== actual) {
        result.push(actual);
        last = actual;
      }
    }
  }

  return result;
}