"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = swap;
function swap(array) {
    var result = {};
    for (var i = 0; i < array.length; i++) {
        result[array[i]] = i;
    }

    return result;
}