'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _urijs = require('urijs');

var _urijs2 = _interopRequireDefault(_urijs);

var _intersect = require('./intersect');

var _intersect2 = _interopRequireDefault(_intersect);

var _union = require('./union');

var _union2 = _interopRequireDefault(_union);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MODE_INTERSECT = 'intersect';
var MODE_UNION = 'union';

var SELECT_MODE_RADIO = 'radio'; // vzdy jedna hodnota ktora sa neda deaktivovat
var SELECT_MODE_CHECKBOX = 'checkbox'; // lubovolny pocet hodnot

/**
 * Reprezentuje filter
 */

var Filter = function () {
  function Filter(name) {
    var container = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    _classCallCheck(this, Filter);

    this.name = name;
    this.mode = MODE_UNION;

    this.enabled = true;

    this.container = container; // kontainer do ktoreho sa vykresli filter

    this.values = {}; // kazda value ma svoje id ako key a musi obsahovat property 'id', 'ids' a 'title'
    this.activeValues = {}; // aktivne hodnoty filtra
    this.filteredValues = []; // zoradene pole aktualnych hodnot filtra
    this.renderValue = function (value, container, toggleCallback, active, filteredCount, count, index) {}; // funkce vykreslujuca 1 hodnotu filtra

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.selectMode = SELECT_MODE_CHECKBOX; // urcuje mod pre selektovanie hodnot

    this.sortEnabled = true; // urcije zda sa maju hodntoy filtra zoradit
    this.sortByActive = true;
    this.sortByCount = true;
    this.sortPropName = 'title';

    this.showEmptyValues = false; // urcije ci sa maju zobrazovat prazdne hodnoty
    this.valuesDelimiter = ','; // oddelovac pre hodnoty filtra v url
    this.defaultValueId = null; // ak je nastavena defaultna hodnota neprepisuje sa do URL

    this.onChange = []; // callbacky funkci zavolanych po invalidacii filtra

    this.combineFunctions = {}; // podla aktualneho modu neskor bude vyberat kombinacnu funkciu
    this.combineFunctions[MODE_INTERSECT] = _intersect2.default;
    this.combineFunctions[MODE_UNION] = _union2.default;

    this.moreLimit = null; // pocet prvkov pri skrytom filtre (ak je 'null' zobrazuje vsetky)
    this.moreStep = 20;
    this.renderMoreButton = function (count, container, moreCallback) {}; // funkce vykreslujuca tlacitko pre zobrazenie viacerych vysledkov

    this.filteredIds = undefined;

    this.idsPropName = 'ids';
    this.valueIdPropName = 'id';

    Object.assign(this, options); // nastavi hodnoty pri inicializaci
  }

  _createClass(Filter, [{
    key: 'setEnabled',
    value: function setEnabled(enabled) {
      this.enabled = enabled;

      return this;
    }
  }, {
    key: 'reset',
    value: function reset() {
      var _this = this;

      Object.keys(this.activeValues).forEach(function (valueId) {
        delete _this.activeValues[valueId].active;
        delete _this.activeValues[valueId];
      });

      if (this.defaultValueId && this.values[this.defaultValueId]) {
        this.toggleValue(this.defaultValueId);
      }

      this.filteredIds = undefined;

      this.updateUrl();

      return this;
    }

    /**
     * Inicializuje hodnoty filtra
     *
     * @param {Object} data objekt obsahujuci property 'values' a 'activeValueIds'
     * @returns {Filter}
     */

  }, {
    key: 'setData',
    value: function setData(data) {
      var _this2 = this;

      this.values = data.values;

      data.activeValueIds.forEach(function (valueId) {
        if (_this2.values[valueId]) {
          _this2.toggleValue(valueId);
        }
      });

      if (this.defaultValueId && !Object.keys(this.activeValues).length && this.values[this.defaultValueId]) {
        this.toggleValue(this.defaultValueId);
      }

      return this;
    }

    /**
     * Aktivuje / deaktivuje hodnotu filtra
     *
     * @param {String} valueId identifikator hodnoty filtra
     * @returns {Filter}
     */

  }, {
    key: 'toggleValue',
    value: function toggleValue(valueId) {
      var _this3 = this;

      if (this.activeValues[valueId]) {
        // je aktivna hodnota
        if (this.selectMode != SELECT_MODE_RADIO) {
          // ak je radio tak nemoze deaktivaovtt samu seba
          delete this.activeValues[valueId].active;
          delete this.activeValues[valueId]; // odstrani z mnoziny aktivnych
        }
      } else {
        if (this.selectMode == SELECT_MODE_RADIO) {
          // ak neni povoleno viacero hodnot oznacime aktivne na neaktivne
          Object.keys(this.activeValues).forEach(function (valueId) {
            delete _this3.activeValues[valueId].active;
            delete _this3.activeValues[valueId];
          });
        }

        this.activeValues[valueId] = this.values[valueId];
        this.activeValues[valueId].active = true;
      }

      this.filteredIds = undefined;

      this.updateUrl();

      return this;
    }

    /**
     * Nastavy parametre filtra do url bez reloadu stranky
     *
     * @returns {Filter}
     */

  }, {
    key: 'updateUrl',
    value: function updateUrl() {
      var uri = new _urijs2.default(window.location.href);
      var query = Object.keys(this.activeValues).join(this.urlValuesDelimiter);

      if (query != '' && query != this.defaultValueId) {
        // neni prazdnu query a nieje defaultna hodnota
        uri.setQuery('f[' + this.name + ']', query);
      } else {
        uri.removeQuery('f[' + this.name + ']');
      }

      window.history.pushState(null, null, uri.toString());

      return this;
    }

    /**
     * Zoradi hodnoty filtra
     *
     * @param {String} valueId identifikator hodnoty filtra
     * @returns {Filter}
     */

  }, {
    key: 'sortFilteredValues',
    value: function sortFilteredValues() {
      var _this4 = this;

      this.filteredValues.sort(function (a, b) {

        // aktivny ma vacsiu prioritu
        if (_this4.sortByActive && a.active != b.active) {
          return a.active ? -1 : 1;
        }

        if (_this4.sortByCount) {
          if (a[_this4.idsPropName] && b[_this4.idsPropName]) {
            // radime podla poctu vysledkov
            var res = void 0;
            if ((res = b[_this4.idsPropName].length - a[_this4.idsPropName].length) !== 0) {
              return res;
            }
          } else if (a[_this4.idsPropName]) {
            // b je null - nefiltrujhe nic tak ma najviac vysledkov
            return 1;
          } else {
            // a je null
            return -1;
          }
        }

        // nakoniec radime podla titulku
        return a[_this4.sortPropName] < b[_this4.sortPropName] ? -1 : a[_this4.sortPropName] > b[_this4.sortPropName] ? 1 : 0;
      });

      return this;
    }

    /**
     * Invaliduje aktualny filter a vyvola 'onChange' event
     *
     * @returns {Filter}
     */

  }, {
    key: 'invalidate',
    value: function invalidate() {
      this.onChange.forEach(function (callback) {
        callback.call();
      });

      return this;
    }

    /**
     * Invaliduje aktualny filter a vyvola 'onChange' event
     *
     * @param {Array} onlyIds - mnozina dostupnych ideciek
     * @returns {Filter}
     */

  }, {
    key: 'filterValues',
    value: function filterValues() {
      var onlyIds = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;


      if (this.mode == MODE_INTERSECT) {
        // ak je v rezime prieniku ovplyvnuje dostupne idecka
        var filteredIds = this.getFilteredIds();

        if (!onlyIds) {
          onlyIds = filteredIds;
        } else if (filteredIds) {
          // filtruje a je nastavena obmezdena mnozina
          onlyIds = (0, _intersect2.default)(filteredIds, onlyIds);
        }
      }

      // inicializujeme vyfiltrovane hodnoty
      this.filteredValues = [];

      if (onlyIds) {
        // obmedzena (vyfiltrovana) mnozina ideciek
        var valueFilteredIds = void 0;

        for (var valueId in this.values) {
          // pre kazdu hodnotu spravime prienik valueIdeciek s dostupnymi valueIdeckami
          // ak hodnota filtruje
          if (valueFilteredIds = this.values[valueId][this.idsPropName]) {
            valueFilteredIds = (0, _intersect2.default)(valueFilteredIds, onlyIds);
          }

          if (!valueFilteredIds || valueFilteredIds.length > 0 || this.values[valueId].active || this.showEmptyValues) {
            // prienik nieje prazdny alebo je hodnota aktivna alebo je nastavene zobrazovanie praznych hodnot
            this.filteredValues.push(Object.assign({}, this.values[valueId], _defineProperty({}, this.idsPropName, valueFilteredIds))); // vytvorime kopiu hodnoty aby neovplyvnilo vychozi stav
          }
        }
      } else {
        // iba zobrazime aktualne hodnoty
        for (var _valueId in this.values) {
          this.filteredValues.push(this.values[_valueId]); // nemusime kopirovat lebo sa nemeni stav ideciek
        }
      }

      // zoradime hodnoty filtra
      if (this.sortEnabled) {
        this.sortFilteredValues();
      }

      return this;
    }

    /**
     * Vykresli filter
     *
     * @returns {Filter}
     */

  }, {
    key: 'render',
    value: function render() {
      var _this5 = this;

      this.onRenderStart.forEach(function (callback) {
        callback(_this5.enabled);
      });

      var valuesCount = this.filteredValues.length;

      this.container.innerHTML = '';

      var limit = this.moreLimit ? // je nastaveny limit a nezobrazujeme vsekty
      Math.min(valuesCount, this.moreLimit) : // celkovy pocet moze byt mensi
      valuesCount;

      // vykreslime hodnoty filtra
      this.filteredValues.slice(0, limit).forEach(function (value, index) {
        var toggleCallback = function toggleCallback(event) {
          // pridame event pre kliknutie na hodnotu
          event.preventDefault();
          _this5.toggleValue(value[_this5.valueIdPropName]).invalidate();
        };

        _this5.renderValue(value, _this5.container, toggleCallback, !!value.active, value[_this5.idsPropName] && value[_this5.idsPropName].length, _this5.values[value[_this5.valueIdPropName]][_this5.idsPropName] && _this5.values[value[_this5.valueIdPropName]][_this5.idsPropName].length, index);
      });

      // vykreslime tlacitko na zobrazenie viacerych hodnot
      if (this.moreLimit && this.moreLimit < valuesCount) {
        this.renderMoreButton(valuesCount - this.moreLimit, this.container, function (event) {
          // pridame event pre kliknutie na tlacitko
          event.preventDefault();
          _this5.moreLimit += _this5.moreStep;
          _this5.render();
        });
      }

      this.onRenderEnd.forEach(function (callback) {
        callback(_this5.enabled, Object.keys(_this5.activeValues).length);
      });

      return this;
    }

    /**
     * Vyfiltruje a vrati idecka splnujuce filter
     *
     * @returns {Array|null} vrati 'null' ak nefiltruje nic
     */

  }, {
    key: 'getFilteredIds',
    value: function getFilteredIds() {
      if (!this.enabled) {
        return this.filteredIds = null;
      }

      if (this.filteredIds === undefined) {
        // vysledok v cache

        var filteredIds = null;
        for (var valueId in this.activeValues) {
          filteredIds = filteredIds ? this.combineFunctions[this.mode](filteredIds, this.activeValues[valueId][this.idsPropName]) : // skombinuje hodnoty
          this.activeValues[valueId][this.idsPropName]; // inicializuje
        }

        this.filteredIds = filteredIds;
      }

      return this.filteredIds;
    }
  }]);

  return Filter;
}();

exports.default = Filter;