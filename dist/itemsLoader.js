'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _urijs = require('urijs');

var _urijs2 = _interopRequireDefault(_urijs);

var _intersect = require('./intersect');

var _intersect2 = _interopRequireDefault(_intersect);

var _union = require('./union');

var _union2 = _interopRequireDefault(_union);

var _swap = require('./swap');

var _swap2 = _interopRequireDefault(_swap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var STATE_INIT = 'init';
var STATE_READY = 'ready';
var STATE_LOADING = 'loading';
var STATE_END = 'end';

/**
 * Sluzi na kombinovanie lubovolneho poctu filtrov
 */

var ItemsLoader = function () {
  function ItemsLoader(container) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, ItemsLoader);

    this.container = container;

    this.loadCallback = function (data, start, complete, success) {};

    this.filters = {};
    this.sort = null;
    this.sortDataPropName = 'sort';

    this.pageOffset = 0;
    this.pageKey = 'p';
    this.loadedPage = 0;

    this.loadedIds = null;
    this.filteredIds = null;

    this.state = STATE_INIT;
    this.loadLimit = 10;
    this.scrollLimitHeight = 1000;

    this.scrollEvent;

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.onLoadStart = [];
    this.onLoadEnd = [];

    this.onScroll = [];

    Object.assign(this, options); // nastavi hodnoty pri inicializaci
  }

  /**
   * Prida filter do zoznamu filtrov
   *
   * @param {Filter} filter
   * @returns FilterList
   */


  _createClass(ItemsLoader, [{
    key: 'addFilter',
    value: function addFilter(filter) {
      var _this = this;

      this.filters[filter.name] = filter;

      filter.onChange.push(function () {
        _this.validate().loadItems();
      });

      return this;
    }
  }, {
    key: 'setSort',
    value: function setSort(sort) {
      var _this2 = this;

      this.sort = sort;

      if (sort) {
        sort.onChange.push(function () {
          _this2.validate().loadItems();
        });
      }

      return this;
    }
  }, {
    key: 'reset',
    value: function reset() {
      for (var filterName in this.filters) {
        this.filters[filterName].reset();
      }

      return this;
    }

    /**
     * Zvaliduje aktualny zoznam filtrov a vykresli
     *
     * @param {Filter} filter
     * @returns FilterList
     */

  }, {
    key: 'validate',
    value: function validate() {

      // aktivuje / deaktivuje filtry
      if (this.sort) {
        var disabledFilters = (0, _swap2.default)(this.sort.getDisabledFilters());

        for (var filterName in this.filters) {
          this.filters[filterName].setEnabled(!disabledFilters.hasOwnProperty(filterName));
        }
      }

      this.onRenderStart.forEach(function (callback) {
        callback();
      });

      // vykresli filtry
      for (var _filterName in this.filters) {
        this.filters[_filterName].filterValues(this.getFilteredIds(this.filters[_filterName])).render();
      }

      // vykresli razeni
      if (this.sort) {
        this.sort.render();
      }

      this.onRenderEnd.forEach(function (callback) {
        callback();
      });

      // inicializuejem nacitavanie
      this.loadedPage = 0;
      this.loadedIds = null;
      this.filteredIds = this.getFilteredIds();

      if (this.sort) {
        this.filteredIds = this.sort.applySort(this.filteredIds); // zoradi vyfiltrovane idecka (ak sa nic nefiltruje tak vrati idecka z razeni)
      }

      this.state = STATE_INIT;

      return this;
    }
  }, {
    key: 'getItemsCount',
    value: function getItemsCount() {
      return this.filteredIds.length;
    }
  }, {
    key: 'bindScroll',
    value: function bindScroll() {
      var _this3 = this;

      if (this.state == STATE_INIT) {
        this.state = STATE_READY;

        if (this.scrollEvent) {
          document.removeEventListener('scroll', this.scrollEvent);
        }

        this.scrollEvent = function (event) {
          var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
          if (_this3.state == STATE_READY && getComputedStyle(_this3.container).display !== 'none' && _this3.container.offsetHeight + _this3.container.offsetTop - (scrollTop + window.innerHeight) < _this3.scrollLimitHeight) {
            _this3.loadItems(1, true);
          }

          _this3.onScroll.forEach(function (callback) {
            callback(scrollTop);
          });
        };

        document.addEventListener('scroll', this.scrollEvent);
      }

      return this;
    }
  }, {
    key: 'unbindScroll',
    value: function unbindScroll() {
      if (this.scrollEvent) {
        document.removeEventListener('scroll', this.scrollEvent);

        this.scrollEvent = null;
      }

      return this;
    }
  }, {
    key: 'loadItems',
    value: function loadItems() {
      var _this4 = this;

      var pagesCount = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var append = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;


      this.bindScroll();

      var options = {
        append: append
      };

      // ak je v mode razeni tak razeni a strankovani prebieha na strane klienta
      if (this.sort) {
        var start = this.loadedPage * this.loadLimit;
        var end = start + this.loadLimit * pagesCount;

        options.ids = this.filteredIds ? this.filteredIds.slice(start, end) : [];
      } else {
        options.ids = this.filteredIds !== null && this.filteredIds.length == 0 ? [-1] : this.filteredIds; // koli problemu s odosielanim prazdneho pola (null vs. prazdne pole)
        options.excludedIds = this.loadedIds;
        options.limit = this.loadLimit * pagesCount;
      }

      this.loadCallback(options, function () {
        _this4.state = STATE_LOADING;

        _this4.onLoadStart.forEach(function (callback) {
          callback(append); // s priznakom ci sa jedna o nacitavani dalsich
        });
      }, function () {
        if (_this4.state == STATE_LOADING) {
          _this4.state = STATE_READY;
        }
      }, function (data) {
        var loadedIds = data.loadedIds.sort(function (a, b) {
          return a - b;
        }); // preistotu zoradime (potrebuejeme pre 'intersect' zoradene)

        _this4.loadedIds = _this4.loadedIds ? (0, _union2.default)(_this4.loadedIds, loadedIds) : loadedIds;
        _this4.loadedPage += Math.ceil(loadedIds.length / _this4.loadLimit);

        if (loadedIds.length < _this4.loadLimit || loadedIds.length == 0) {
          _this4.state = STATE_END;
        }

        _this4.updateUrl();

        _this4.onLoadEnd.forEach(function (callback) {
          callback(_this4.state !== STATE_END); // zavola callbeck s priznakom od dalsom nacitavani
        });
      });
    }

    /**
     * Vrati vyfiltrovane idecka
     *
     * @param {Filter} excludedFilter - ignorovany filter
     * @returns FilterList
     */

  }, {
    key: 'getFilteredIds',
    value: function getFilteredIds() {
      var excludedFilter = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      var filteredIds = null;

      var actualFilter = void 0;
      var actualFilteredIds = null;

      for (var filterName in this.filters) {
        actualFilter = this.filters[filterName];

        if (actualFilter === excludedFilter) {
          // filter je ignorovany
          continue;
        }

        actualFilteredIds = actualFilter.getFilteredIds();

        if (!filteredIds) {
          // ziadne vyfiltrovane idecka
          filteredIds = actualFilteredIds;
        } else if (actualFilteredIds) {
          // aktualny filter filtruje
          filteredIds = (0, _intersect2.default)(filteredIds, actualFilteredIds); // prienik
        }
      }

      return filteredIds;
    }
  }, {
    key: 'updateUrl',
    value: function updateUrl() {
      if (this.pageKey) {
        var uri = new _urijs2.default(window.location.href);

        var page = this.loadedPage + this.pageOffset;

        if (page > 1) {
          uri.setQuery(this.pageKey, page);
        } else {
          uri.removeQuery(this.pageKey);
        }

        window.history.pushState(null, null, uri.toString());
      }

      return this;
    }
  }, {
    key: 'setData',


    /**
     * Nastavi hodnoty filtrov
     *
     * @param {Object} data
     */
    value: function setData(data) {
      var filter = void 0;
      for (var filterName in data) {
        if (filter = this.filters[filterName]) {
          filter.setData(data[filterName]);
        } else if (filterName === this.sortDataPropName && this.sort) {
          // data pre sort
          this.sort.setData(data[filterName]);
        }
      }

      return this;
    }
  }]);

  return ItemsLoader;
}();

exports.default = ItemsLoader;