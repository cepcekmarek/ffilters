import ItemsLoader from '../../dist/itemsLoader';
import Filter from '../../dist/filter';


let loadCallback = (data, start, complete, success) => {
  console.log(data);
  start();
  success({
    loadedIds: data.ids || []
  });
  complete();
};


let data = {
  a:{
    values: {
      1:{id: 1, ids: [1,2,3], title: 'val1'},
      2:{id: 2, ids: [4,5,6], title: 'val2'}
    },
    activeValueIds: []
  },
  b:{
    values: {
      0:{id: 0, ids: null, title: ''},
      1:{id: 1, ids: [1,2,3,4], title: 'val1'},
      2:{id: 2, ids: [4,5,6], title: 'val2'},
      3:{id: 3, ids: [7,8,9], title: 'val3'}
    },
    activeValueIds: []
  }
};

let loader = new ItemsLoader(document.getElementById('results'), {
  loadCallback: loadCallback
});

let filterA = new Filter('a', document.getElementById('filterA'), {
  renderValue: (value, container, toggleCallback, active, filteredCount, count, index) => {
    let element = document.createElement('span');
    element.innerHTML = value.title;
    container.appendChild(element);
    element.addEventListener('click', toggleCallback);
  }
});

let select = document.getElementById('filterB');
let filterB = new Filter('b', select, {
  selectMode: 'radio',
  // mode: 'intersect',
  renderValue: (value, container, toggleCallback, active, filteredCount, count, index) => {
    let element = document.createElement('option');
    element.value = value.id
    element.innerHTML = value.title;
    container.appendChild(element);
    //element.addEventListener('click', toggleCallback);
  }
});

select.addEventListener('change', () => {
  filterB.toggleValue(select.value).invalidate();
});

loader.addFilter(filterA).addFilter(filterB);

loader.setData(data).validate();
