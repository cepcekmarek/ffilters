
/**
 * Veati prienik zadanych poly bez duplicit (polia musia byt zoradene)
 *
 * @param {Array} a
 * @param {Array} b
 * @returns {Array} prienik poly
 */
export default function intersect(a, b) {
  let i = 0;
  let j = 0;

  let iMax = a.length;
  let jMax = b.length;

  let result = [];
  let last = null;
  let actual;
  while (i < iMax && j < jMax) {
    if (a[i] < b[j]) {
      i++;
    } else if (a[i] > b[j]){
      j++;
    } else {
      actual = a[i++];
      j++;

      if (last === null || last !== actual) {
        result.push(actual);
        last = actual;
      }
    }
  }

  return result;
}
