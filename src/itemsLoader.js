import URI from 'urijs';

import intersect from './intersect';
import union from './union';
import swap from './swap';

const STATE_INIT = 'init';
const STATE_READY = 'ready';
const STATE_LOADING = 'loading';
const STATE_END = 'end';

/**
 * Sluzi na kombinovanie lubovolneho poctu filtrov
 */
export default class ItemsLoader {
  constructor (container, options = {}) {
    this.container = container;

    this.loadCallback = (data, start, complete, success) => {};

    this.filters = {};
    this.sort = null;
    this.sortDataPropName = 'sort';

    this.pageOffset = 0;
    this.pageKey = 'p';
    this.loadedPage = 0;

    this.loadedIds = null;
    this.filteredIds = null;

    this.state = STATE_INIT;
    this.loadLimit = 10;
    this.scrollLimitHeight = 1000;

    this.scrollEvent;

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.onLoadStart = [];
    this.onLoadEnd = [];

    this.onScroll = [];

    Object.assign(this, options); // nastavi hodnoty pri inicializaci
  }

  /**
   * Prida filter do zoznamu filtrov
   *
   * @param {Filter} filter
   * @returns FilterList
   */
  addFilter(filter) {
    this.filters[filter.name] = filter;

    filter.onChange.push(() => {
      this.validate().loadItems();
    });

    return this;
  }

  setSort(sort) {
    this.sort = sort;

    if (sort) {
      sort.onChange.push(() => {
        this.validate().loadItems();
      });
    }

    return this;
  }

  reset() {
    for (let filterName in this.filters) {
      this.filters[filterName].reset();
    }

    return this;
  }

  /**
   * Zvaliduje aktualny zoznam filtrov a vykresli
   *
   * @param {Filter} filter
   * @returns FilterList
   */
  validate() {
    
    // aktivuje / deaktivuje filtry
    if (this.sort) {
      let disabledFilters = swap(this.sort.getDisabledFilters());

      for (let filterName in this.filters) {
        this.filters[filterName].setEnabled(!disabledFilters.hasOwnProperty(filterName));
      }
    }

    this.onRenderStart.forEach((callback) => {
      callback();
    });

    // vykresli filtry
    for (let filterName in this.filters) {
      this.filters[filterName].filterValues(this.getFilteredIds(this.filters[filterName])).render();
    }

    // vykresli razeni
    if (this.sort) {
      this.sort.render();
    }

    this.onRenderEnd.forEach((callback) => {
      callback();
    });

    // inicializuejem nacitavanie
    this.loadedPage = 0;
    this.loadedIds = null;
    this.filteredIds = this.getFilteredIds();

    if (this.sort) {
      this.filteredIds = this.sort.applySort(this.filteredIds); // zoradi vyfiltrovane idecka (ak sa nic nefiltruje tak vrati idecka z razeni)
    }

    this.state = STATE_INIT;

    return this;
  }

  getItemsCount() {
    return this.filteredIds.length;
  }

  bindScroll() {
    if (this.state == STATE_INIT) {
      this.state = STATE_READY;

      if (this.scrollEvent) {
        document.removeEventListener('scroll', this.scrollEvent);
      }

      this.scrollEvent = (event) => {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        if (
          this.state == STATE_READY &&
          getComputedStyle(this.container).display !== 'none' &&
          (this.container.offsetHeight + this.container.offsetTop) - (scrollTop + window.innerHeight) < this.scrollLimitHeight
        ) {
          this.loadItems(1, true);
        }

        this.onScroll.forEach((callback) => {
          callback(scrollTop);
        });
      };

      document.addEventListener('scroll', this.scrollEvent);
    }

    return this;
  }

  unbindScroll() {
    if (this.scrollEvent) {
      document.removeEventListener('scroll', this.scrollEvent);

      this.scrollEvent = null;
    }

    return this;
  }

  loadItems(pagesCount = 1, append = false) {

    this.bindScroll();

    let options = {
      append: append
    };

    // ak je v mode razeni tak razeni a strankovani prebieha na strane klienta
    if (this.sort) {
      let start = this.loadedPage * this.loadLimit;
      let end = start + this.loadLimit * pagesCount;

      options.ids = this.filteredIds ? this.filteredIds.slice(start, end) : [];
    } else {
      options.ids = this.filteredIds !== null && this.filteredIds.length == 0 ? [-1] : this.filteredIds; // koli problemu s odosielanim prazdneho pola (null vs. prazdne pole)
      options.excludedIds = this.loadedIds;
      options.limit = this.loadLimit * pagesCount;
    }

    this.loadCallback(
      options,
      () => {
        this.state = STATE_LOADING;

        this.onLoadStart.forEach((callback) => {
          callback(append); // s priznakom ci sa jedna o nacitavani dalsich
        });
      },
      () => {
        if (this.state == STATE_LOADING) {
          this.state = STATE_READY;
        }
      },
      (data) => {
        let loadedIds = data.loadedIds.sort((a, b) => a - b); // preistotu zoradime (potrebuejeme pre 'intersect' zoradene)

        this.loadedIds = this.loadedIds ? union(this.loadedIds, loadedIds) : loadedIds;
        this.loadedPage += Math.ceil(loadedIds.length / this.loadLimit);

        if (loadedIds.length < this.loadLimit || loadedIds.length == 0) {
          this.state = STATE_END;
        }

        this.updateUrl();

        this.onLoadEnd.forEach((callback) => {
          callback(this.state !== STATE_END); // zavola callbeck s priznakom od dalsom nacitavani
        });
      }
    );
  }

  /**
   * Vrati vyfiltrovane idecka
   *
   * @param {Filter} excludedFilter - ignorovany filter
   * @returns FilterList
   */
  getFilteredIds(excludedFilter = null) {
    let filteredIds = null;

    let actualFilter;
    let actualFilteredIds = null;

    for (let filterName in this.filters) {
      actualFilter = this.filters[filterName];

      if (actualFilter === excludedFilter) { // filter je ignorovany
        continue;
      }

      actualFilteredIds = actualFilter.getFilteredIds();

      if (!filteredIds) { // ziadne vyfiltrovane idecka
        filteredIds = actualFilteredIds;
      } else if (actualFilteredIds) { // aktualny filter filtruje
        filteredIds = intersect(filteredIds, actualFilteredIds); // prienik
      }
    }

    return filteredIds;
  }

  updateUrl() {
    if (this.pageKey) {
      let uri = new URI(window.location.href);

      let page = this.loadedPage + this.pageOffset;

      if (page > 1) {
        uri.setQuery(this.pageKey, page);
      } else {
        uri.removeQuery(this.pageKey);
      }

      window.history.pushState(null, null, uri.toString());
    }

    return this;
  };

  /**
   * Nastavi hodnoty filtrov
   *
   * @param {Object} data
   */
  setData(data) {
    let filter;
    for (let filterName in data) {
      if (filter = this.filters[filterName]) {
        filter.setData(data[filterName]);
      } else if (filterName === this.sortDataPropName && this.sort) { // data pre sort
        this.sort.setData(data[filterName]);
      }
    }

    return this;
  }
}
