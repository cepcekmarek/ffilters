import ItemsLoader from './itemsLoader';
import Filter from './filter';

export default {
  ItemsLoader: ItemsLoader,
  Filter: Filter
}
