import URI from 'urijs';

import intersect from './intersect';
import union from './union';

const MODE_INTERSECT = 'intersect';
const MODE_UNION = 'union';

const SELECT_MODE_RADIO = 'radio'; // vzdy jedna hodnota ktora sa neda deaktivovat
const SELECT_MODE_CHECKBOX = 'checkbox'; // lubovolny pocet hodnot

/**
 * Reprezentuje filter
 */
export default class Filter {
  constructor(name, container = null, options = {}) {
    this.name = name;
    this.mode = MODE_UNION;

    this.enabled = true;

    this.container = container; // kontainer do ktoreho sa vykresli filter

    this.values = {}; // kazda value ma svoje id ako key a musi obsahovat property 'id', 'ids' a 'title'
    this.activeValues = {}; // aktivne hodnoty filtra
    this.filteredValues = []; // zoradene pole aktualnych hodnot filtra
    this.renderValue = (value, container, toggleCallback, active, filteredCount, count, index) => {}; // funkce vykreslujuca 1 hodnotu filtra

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.selectMode = SELECT_MODE_CHECKBOX; // urcuje mod pre selektovanie hodnot

    this.sortEnabled = true; // urcije zda sa maju hodntoy filtra zoradit
    this.sortByActive = true;
    this.sortByCount = true;
    this.sortPropName = 'title';

    this.showEmptyValues = false; // urcije ci sa maju zobrazovat prazdne hodnoty
    this.valuesDelimiter = ','; // oddelovac pre hodnoty filtra v url
    this.defaultValueId = null; // ak je nastavena defaultna hodnota neprepisuje sa do URL

    this.onChange = []; // callbacky funkci zavolanych po invalidacii filtra

    this.combineFunctions = {}; // podla aktualneho modu neskor bude vyberat kombinacnu funkciu
    this.combineFunctions[MODE_INTERSECT] = intersect;
    this.combineFunctions[MODE_UNION] = union;

    this.moreLimit = null; // pocet prvkov pri skrytom filtre (ak je 'null' zobrazuje vsetky)
    this.moreStep = 20;
    this.renderMoreButton = (count, container, moreCallback) => {}; // funkce vykreslujuca tlacitko pre zobrazenie viacerych vysledkov

    this.filteredIds = undefined;

    this.idsPropName = 'ids';
    this.valueIdPropName = 'id';

    Object.assign(this, options); // nastavi hodnoty pri inicializaci
  }

  setEnabled(enabled) {
    this.enabled = enabled;

    return this;
  }

  reset() {
    Object.keys(this.activeValues).forEach((valueId) => {
      delete this.activeValues[valueId].active;
      delete this.activeValues[valueId];
    });

    if (this.defaultValueId && this.values[this.defaultValueId]) {
      this.toggleValue(this.defaultValueId);
    }

    this.filteredIds = undefined;

    this.updateUrl();

    return this;
  }

  /**
   * Inicializuje hodnoty filtra
   *
   * @param {Object} data objekt obsahujuci property 'values' a 'activeValueIds'
   * @returns {Filter}
   */
  setData(data) {
    this.values = data.values;

    data.activeValueIds.forEach((valueId) => {
      if (this.values[valueId]) {
        this.toggleValue(valueId);
      }
    });

    if (this.defaultValueId && !Object.keys(this.activeValues).length && this.values[this.defaultValueId]) {
      this.toggleValue(this.defaultValueId);
    }

    return this;
  }

  /**
   * Aktivuje / deaktivuje hodnotu filtra
   *
   * @param {String} valueId identifikator hodnoty filtra
   * @returns {Filter}
   */
  toggleValue(valueId) {
    if (this.activeValues[valueId]) { // je aktivna hodnota
      if (this.selectMode != SELECT_MODE_RADIO) { // ak je radio tak nemoze deaktivaovtt samu seba
        delete this.activeValues[valueId].active;
        delete this.activeValues[valueId]; // odstrani z mnoziny aktivnych
      }
    } else {
      if (this.selectMode == SELECT_MODE_RADIO) { // ak neni povoleno viacero hodnot oznacime aktivne na neaktivne
        Object.keys(this.activeValues).forEach((valueId) => {
          delete this.activeValues[valueId].active;
          delete this.activeValues[valueId];
        });
      }

      this.activeValues[valueId] = this.values[valueId];
      this.activeValues[valueId].active = true;
    }

    this.filteredIds = undefined;

    this.updateUrl();

    return this;
  }

  /**
   * Nastavy parametre filtra do url bez reloadu stranky
   *
   * @returns {Filter}
   */
  updateUrl() {
    let uri = new URI(window.location.href);
    let query = Object.keys(this.activeValues).join(this.urlValuesDelimiter);

    if (query != '' && query != this.defaultValueId) { // neni prazdnu query a nieje defaultna hodnota
      uri.setQuery('f[' + this.name + ']', query);
    } else {
      uri.removeQuery('f[' + this.name + ']');
    }

    window.history.pushState(null, null, uri.toString());

    return this;
  }

  /**
   * Zoradi hodnoty filtra
   *
   * @param {String} valueId identifikator hodnoty filtra
   * @returns {Filter}
   */
  sortFilteredValues() {
    this.filteredValues.sort((a, b) => {

      // aktivny ma vacsiu prioritu
      if (this.sortByActive && a.active != b.active) {
        return a.active ? -1 : 1;
      }

      if (this.sortByCount) {
        if (a[this.idsPropName] && b[this.idsPropName]) {
          // radime podla poctu vysledkov
          let res;
          if ((res = b[this.idsPropName].length - a[this.idsPropName].length) !== 0) {
            return res;
          }
        } else if (a[this.idsPropName]) { // b je null - nefiltrujhe nic tak ma najviac vysledkov
          return 1;
        } else { // a je null
          return -1;
        }
      }

      // nakoniec radime podla titulku
      return a[this.sortPropName] < b[this.sortPropName] ? -1 : (a[this.sortPropName] > b[this.sortPropName] ? 1 : 0);
    });

    return this;
  }

  /**
   * Invaliduje aktualny filter a vyvola 'onChange' event
   *
   * @returns {Filter}
   */
  invalidate() {
    this.onChange.forEach(function (callback) {
      callback.call();
    });

    return this;
  }

  /**
   * Invaliduje aktualny filter a vyvola 'onChange' event
   *
   * @param {Array} onlyIds - mnozina dostupnych ideciek
   * @returns {Filter}
   */
  filterValues(onlyIds = null) {

    if (this.mode == MODE_INTERSECT) { // ak je v rezime prieniku ovplyvnuje dostupne idecka
      let filteredIds = this.getFilteredIds();

      if (!onlyIds) {
        onlyIds = filteredIds;
      } else if (filteredIds) { // filtruje a je nastavena obmezdena mnozina
        onlyIds = intersect(filteredIds, onlyIds);
      }
    }

    // inicializujeme vyfiltrovane hodnoty
    this.filteredValues = [];

    if (onlyIds) { // obmedzena (vyfiltrovana) mnozina ideciek
      let valueFilteredIds;

      for (let valueId in this.values) { // pre kazdu hodnotu spravime prienik valueIdeciek s dostupnymi valueIdeckami
        // ak hodnota filtruje
        if (valueFilteredIds = this.values[valueId][this.idsPropName]) {
          valueFilteredIds = intersect(valueFilteredIds, onlyIds);
        }

        if (!valueFilteredIds || valueFilteredIds.length > 0 || this.values[valueId].active || this.showEmptyValues) { // prienik nieje prazdny alebo je hodnota aktivna alebo je nastavene zobrazovanie praznych hodnot
          this.filteredValues.push(Object.assign({}, this.values[valueId], { [this.idsPropName]: valueFilteredIds })) // vytvorime kopiu hodnoty aby neovplyvnilo vychozi stav
        }
      }
    } else { // iba zobrazime aktualne hodnoty
      for (let valueId in this.values) {
        this.filteredValues.push(this.values[valueId]); // nemusime kopirovat lebo sa nemeni stav ideciek
      }
    }

    // zoradime hodnoty filtra
    if (this.sortEnabled) {
      this.sortFilteredValues();
    }

    return this;
  }

  /**
   * Vykresli filter
   *
   * @returns {Filter}
   */
  render() {
    this.onRenderStart.forEach((callback) => {
      callback(this.enabled);
    });

    let valuesCount = this.filteredValues.length;

    this.container.innerHTML = '';

    let limit = this.moreLimit ? // je nastaveny limit a nezobrazujeme vsekty
      Math.min(valuesCount, this.moreLimit) : // celkovy pocet moze byt mensi
      valuesCount;

    // vykreslime hodnoty filtra
    this.filteredValues.slice(0, limit).forEach((value, index) => {
      let toggleCallback = (event) => { // pridame event pre kliknutie na hodnotu
        event.preventDefault();
        this.toggleValue(value[this.valueIdPropName])
          .invalidate();
      };

      this.renderValue(
        value,
        this.container,
        toggleCallback,
        !!value.active,
        value[this.idsPropName] && value[this.idsPropName].length,
        this.values[value[this.valueIdPropName]][this.idsPropName] && this.values[value[this.valueIdPropName]][this.idsPropName].length,
        index
      );
    });

    // vykreslime tlacitko na zobrazenie viacerych hodnot
    if (this.moreLimit && this.moreLimit < valuesCount) {
      this.renderMoreButton(valuesCount - this.moreLimit, this.container, (event) => { // pridame event pre kliknutie na tlacitko
        event.preventDefault();
        this.moreLimit += this.moreStep;
        this.render();
      });
    }

    this.onRenderEnd.forEach((callback) => {
      callback(this.enabled, Object.keys(this.activeValues).length);
    });

    return this;
  }

  /**
   * Vyfiltruje a vrati idecka splnujuce filter
   *
   * @returns {Array|null} vrati 'null' ak nefiltruje nic
   */
  getFilteredIds() {
    if (!this.enabled) {
      return this.filteredIds = null;
    }

    if (this.filteredIds === undefined) { // vysledok v cache

      let filteredIds = null;
      for (let valueId in this.activeValues) {
        filteredIds = filteredIds ?
          this.combineFunctions[this.mode](filteredIds, this.activeValues[valueId][this.idsPropName]) : // skombinuje hodnoty
          this.activeValues[valueId][this.idsPropName]; // inicializuje
      }

      this.filteredIds = filteredIds;
    }

    return this.filteredIds;
  }
}
