export default function swap(array) {
    let result = {};
    for(let i = 0; i < array.length; i++) {
      result[array[i]] = i;
    }

    return result;
}

