import URI from 'urijs';

import swap from './swap';

export default class Sort {
  constructor(container, options = {}) {

    this.values = [];
    this.valuesMap = {};

    this.container = container;

    this.renderValue = (title, active) => title;

    this.onChange = [];

    this.defaultValueId = null;
    this.activeValueId = null;

    this.idsPropName = 'ids';
    this.valueIdPropName = 'id';

    this.onRenderStart = [];
    this.onRenderEnd = [];

    this.setData(options); // nastavi hodnoty pri inicializaci
  }

  setData(data) {
    Object.assign(this, data);

    this.valuesMap = {};
    this.values.forEach((value) => {
      this.valuesMap[value[this.valueIdPropName]] = value;
    });

    return this;
  }

  getActiveValue() {
    return this.valuesMap[this.getActiveValueId()];
  }

  getActiveValueId() {
    return this.activeValueId || this.defaultValueId;
  }

  getDisabledFilters() {
    return this.valuesMap[this.getActiveValueId()].disabledFilters || [];
  }

  setActiveValueId(valueId) {
    this.activeValueId = valueId;

    return this;
  }

  updateUrl() {
    let activeValueId = this.getActiveValueId();
    let uri = new URI(window.location.href);

    if (activeValueId != this.defaultValueId) { // neni prazdnu query a nieje defaultna hodnota
      uri.setQuery('s', activeValueId);
    } else {
      uri.removeQuery('s');
    }

    window.history.pushState(null, null, uri.toString());

    return this;
  }

  invalidate() {
    this.onChange.forEach(function (callback) {
      callback();
    });

    return this;
  }

  applySort(values) {
    let activeValue = this.getActiveValue();

    if (!activeValue) { // neni nastavena aktivna hodnota nic nezoradi
      return values;
    }

    if (!values) {
      return activeValue[this.idsPropName]; // neni su nastavene vstupne data tak vrati vsetky idecka z razeni
    }

    let swapedValues = activeValue.swapedValues || (activeValue.swapedValues = swap(activeValue[this.idsPropName]));

    values = values.slice(); // naklonuje pole aby nemodifikovalo povodne

    values.sort((a, b) => {
      return swapedValues[a] - swapedValues[b];
    });
    
    return values;
  }

  render() {
    this.onRenderStart.forEach((callback) => {
      callback();
    });

    let activeValueId = this.getActiveValueId();

    this.container.innerHTML = '';

    this.values.forEach((value) => {
      let active = value[this.valueIdPropName] == activeValueId;

      let sortCallback = (event) => {
        event.preventDefault();
        if (!active) {
          this.setActiveValueId(value[this.valueIdPropName])
            .updateUrl()
            .invalidate()
            .render();
        }
      };

      this.renderValue(value, this.container, sortCallback, active);
    });

    this.onRenderEnd.forEach((callback) => {
      callback();
    });

    return this;
  }
}
