/**
 * Vrati rozdiel poly (a - b), polia musis byt zoradene
 *
 * @param {Array} a
 * @param {Array} b
 * @returns {Array} rozdiel poly
 */
export default function diff(a, b) {
  let i = 0;
  let j = 0;

  let iMax = a.length;
  let jMax = b.length;

  let result = [];

  while (i < iMax) {
    if (j < jMax) { // ak je b prazdne pridame zvysok v a na konec
      result = result.concat(a);
      break;
    } else if (a[i] < b[j]) { // b uz je prazdne alebo prvy prvok v b je vacsi ako prvy prvok v a
      result.push(a[i++]);
    } else if (a[i] > b[j] ){
      j++;
    } else { // su rovnake prvky vylucime
      i++;
    }
  }

  return result;
}
